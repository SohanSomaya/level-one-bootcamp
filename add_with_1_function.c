//Write a program to add two user input numbers using one function.
#include<stdio.h>
int main()
{
	int a,b,sum;
	printf("Enter number 1: \n");
	scanf("%d",&a);
	printf("Enter number 2: \n");
	scanf("%d",&b);
	sum=a+b;
	printf("The sum of %d and %d is: %d\n",a,b,sum);
	return 0;
}
