#include<stdio.h>
#include<math.h>
struct pt
{
    float x;
    float y;
};
typedef struct pt loc;
loc input()
{
	loc a;
	float x, y;
	printf("Enter the x-coordinate of the point: \n");
	scanf("%f",&a.x);
	printf("Enter the y-coordinate of the point: \n");
	scanf("%f",&a.y);
	return a;
}
float calc(loc a, loc b)
{
	return(sqrt(((b.x-a.x)*(b.x-a.x))+((b.y-a.y)*(b.y-a.y))));
}
void res(loc a, loc b, float dist)
{
	printf("The distance between the first point having x coordinate %f and y coordinate %f, and the second point having x coordinate %f and y coordinate %f is %f\n",a.x,a.y,b.x,b.y,dist);
}

int main()
{
	loc a,b;
	printf("Enter the x and y coordinates of the first point: \n");
	a= input();
	printf("Enter the x and y coordinates of the second point: \n");
	b=input();
	float d=calc(a,b);
	res(a,b,d);
	return 0;
}

