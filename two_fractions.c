#include<stdio.h>
typedef struct
{
   int num;
   int denom;
}fract;
 
fract input()
{
	fract a;
	float num, denom;
	printf("Enter the numerator: \n");
	scanf("%d",&a.num);
	printf("Enter the denominator: \n");
	scanf("%d",&a.denom);
	return a;
}

int GCD(int num,int denom)
{
   int i,gcd;
  
   for(i=1;i<=num&&i<=denom;i++)
   {
       if(num%i==0&&denom%i==0)
       {
           gcd = i;
           
          
       }
   }
   return gcd;
}

fract calc(fract a, fract b)
{
   fract c;
   c.num=((a.num*b.denom)+(b.num*a.denom));
   c.denom=a.denom*b.denom;
   int gcd = GCD(c.num,c.denom);
   c.num=c.num/gcd;
   c.denom=c.denom/gcd;
   return c;
 
}
void output(fract a, fract b,fract c)
{
    printf("The sum of %d/%d and %d/%d is %d/%d",a.num,a.denom,b.num,b.denom,c.num,c.denom);
}

int main()
{
   fract a, b, c;
   printf("Enter the details of the first fraction: \n");
   a=input(); 
   printf("Enter the details of the second fraction: \n");
   b=input(); 
   c=calc(a,b);
   output(a,b,c);
   return 0;
}