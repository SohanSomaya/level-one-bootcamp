#include<stdio.h>
#include<math.h>
float input();
float calc(float a, float b, float x, float y);
float res(float a, float b, float x, float y, float d);

float input()
{
	float a;
	scanf("%f",&a);
	return (a);
}
float calc(float a, float b, float x, float y)
{
	return(sqrt((x-a)*(x-a)+(y-b)*(y-b)));
}

float res(float a, float b, float x, float y, float d)
{
	printf("The distance between a point having x axis %f and y axis %f, and another point having x axis %f and y axis %f is %f units\n",a,b,x,y,d);
}

int main()
{
    float a, b, x, y, d;
	printf("Enter x axis of first point: \n");
	a=input();
    printf("Enter y axis of first point: \n");
	b=input();
	printf("Enter x axis of second point: \n");
	x=input();
	printf("Enter y axis of second point: \n");
	y=input();
	d=calc(a,b,x,y);
	res(a,b,x,y,d);
	return (0);
}
