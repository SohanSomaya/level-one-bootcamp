#include<stdio.h>
typedef struct 
{
  int n;
  int d;
}fract;

fract input();
fract compute(int);
fract sum(fract,fract);
int gcd(int,int);
 
fract input()
{
    fract a;
    printf("Enter the numerator:\n");
    scanf("%d",&a.n);
    printf("Enter the denominator:\n");
    scanf("%d",&a.d);
    return a;
 }
 
int gcd(int n, int d)
{
  int i,gcd;
  for(i = 1;i<=n && i<=d; i++)
    {
        if(n%i==0 && d%i==0)
            gcd=i;
    }
    return gcd;
}

fract compute(int n)
{
  int i;
  fract total,a,f[n];
  for(i=0;i<n;i++)
  {
      printf("Enter fraction %d\n",i+1);
      f[i]=input();
  }

  if(n==1)
  {
    total = f[0];
  }
  else
  {
    a = f[0];
    for(i=1;i<n;i++)
    {
        total = sum(a,f[i]);
        a = total;
    }
  }
  return total;
}
 
fract sum(fract f1, fract f2)
{
  fract f3;
  int d;
  f3.n=((f1.n*f2.d)+(f2.n*f1.d));
  f3.d=f1.d*f2.d;
  int c=gcd(f3.n,f3.d);
  f3.n = f3.n/c;
  f3.d = f3.d/c;
  return f3;
}
 
int output(int n,fract total)
{
  printf("The sum of the  %d given fractions : %d / %d\n",n,total.n,total.d);
}

int main()
{
  int n,i;
  fract a,total;
  printf("Enter the number of fractions to add:\n");
  scanf("%d",&n);
  fract f[n];
  total=compute(n);
  output(n,total);
  return 0;
}