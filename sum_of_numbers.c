#include<stdio.h>
int size()
{
    int n;
    printf("Enter the number of elements to be added\n");
    scanf("%d",&n);
    return n;
}

void input(int n, int a[n])
{
	  for(int i=0;i<n;i++)
	  {
	    	printf("Enter element %d:\n",i+1);
		    scanf("%d",&a[i]);
	  }
}

int calc(int n, int a[n])
{
    int sum=0;
    for(int i=0;i<n;i++) 
    {
        sum=sum+a[i];
    }
    return sum;
}

void output(int n,int sum)
{
	 printf("The sum of the %d numbers is %d\n",n,sum);
}

int main()
{
    int n,sum;
    n=size();
    int a[n];
    input(n,a);
    sum=calc(n,a);
    output(n,sum);
    return 0;
}

